<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Customers list</title>
</head>
<body>
	
	<h1 align="center"> List of Customers </h1>
	
	

	<hr/>

		<center>
			<table border="1">
			<tr>
			<th>First Name</th>
			<th>LastName</th>
			<th>email>
			</tr>
			<c:forEach items="${customersList}" var="c">
			<c:url var="updateLink" value="/customer/displayUpdateForm.html">
					<c:param name="customerId" value="${c.id}" />
				</c:url>
 			<c:url var="deleteLink" value="/customer/displayDeleteForm.html">
 				<c:param name="customerId" value="${c.id}"/>
			</c:url>
			<tr>
			<td>${c.firstName}</td>
			<td>${c.lastName}</td>
			<td>${c.email}</td>
			<td><a href="${updateLink}">Update</a> | 
			<a href="${deleteLink}" onclick="if(!(confirm('are you sure you want delete Customer??'))) return false">Delete</a>
			</td>
			</tr>
			</c:forEach>
			</table>
			<button onclick="window.location.href = 'showFormForAdd'; return false;">Add Customer</button>
			</center>
			</body>
			</html>