package com.crud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.crud.entity.Customer;
import com.crud.service.CustomerService;

@Controller
@RequestMapping(value="/customer")
public class CustomerController {
	
	
	@Autowired
	CustomerService customerService;
	
	@RequestMapping(value="/showFormForAdd")
		public String showFormForAdd(Model theModel) {
		theModel.addAttribute("customer", new Customer());
		return "customer-form";
	}
	
	
	@RequestMapping(value="/saveProcess")
		public String saveProcess(@ModelAttribute ("customer") Customer customerObj ) {
		
		customerService.saveCustomer(customerObj);
		
		return "redirect:/customer/list";
	}
	
	
	@RequestMapping(value="/list")
		public String listOfCustomers(Model theModel) {
			theModel.addAttribute("customersList",customerService.getAllCustomers());
			return "list-customers";
		}
	
	@RequestMapping(value="/displayUpdateForm")
		public String displayUpdateForm (@RequestParam ("customerId") int theId, Model theModel) {
		theModel.addAttribute("customer", customerService.getCustomerObj(theId));
		return "customer-form";
		
	}
	
	@RequestMapping(value="/displayDeleteForm")
		public String displayDeleteForm (@RequestParam ("customerId") int theId, Model theModel) {
		customerService.deleteCustomerObject(theId);
		return "redirect:/customer/list";
	}

}
