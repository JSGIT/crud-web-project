package com.crud.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.crud.entity.Customer;

@Repository
public class CustomerDAOImpl implements CustomerDAO{
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public void saveCustomer(Customer customerObj) {
		sessionFactory.getCurrentSession().saveOrUpdate(customerObj);
		
	}

	@Override
	public List<Customer> getAllcustomers() {
		Session currentSession =sessionFactory.getCurrentSession();
		
		List<Customer> customers =currentSession.createQuery("from Customer").getResultList();
		return customers;
	}

	@Override
	public Customer getCustomer(int id) {
		Customer customer =(Customer)sessionFactory.getCurrentSession().get(Customer.class, id);
		
		return customer;
	}

	@Override
	public void deleteCustomer(int id) {
		Customer customerToDelete = (Customer)sessionFactory.getCurrentSession().get(Customer.class, id);
		
		sessionFactory.getCurrentSession().delete(customerToDelete);
		
	}

}
