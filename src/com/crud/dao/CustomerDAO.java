package com.crud.dao;

import java.util.List;

import com.crud.entity.Customer;

public interface CustomerDAO {

	public void saveCustomer(Customer customerObj);
	
	public List<Customer> getAllcustomers();
	
	public Customer getCustomer(int id);
	
	public void deleteCustomer(int id);
	
	
	
}
