package com.crud.service;

import java.util.List;

import com.crud.entity.Customer;

public interface CustomerService {

	public Customer getCustomerObj(int customerId);
	
	public void saveCustomer(Customer customerObject);
	
	public List<Customer> getAllCustomers();
	
	public void deleteCustomerObject(int customerId);
}
