package com.crud.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crud.dao.CustomerDAO;
import com.crud.entity.Customer;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	CustomerDAO customerDAO;
	
	@Override
	@Transactional
	public Customer getCustomerObj(int customerId) {
		return customerDAO.getCustomer(customerId);
		
	}

	@Override
	@Transactional
	public void saveCustomer(Customer customerObject) {
		customerDAO.saveCustomer(customerObject);

	}

	@Override
	@Transactional
	public List<Customer> getAllCustomers() {
		
		return customerDAO.getAllcustomers();
	}

	@Override
	@Transactional
	public void deleteCustomerObject(int customerId) {
		customerDAO.deleteCustomer(customerId);

	}

}
